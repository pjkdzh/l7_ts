import {Component} from "react";
import CSVReader from "react-csv-reader";
import Plot from "./Plot";

const listAuckland = []
const listWellington = []
const listWaikato = []

// te dane trzeba przesłać do plota w 78 linii zamiast list
const handleForce = (data, fileInfo) => {
    console.log(data)

};
const list = [{region: 'Auckland', year: 2007, data_val: 9941.31},
    {region: 'Auckland', year: 2008, data_val: 9510.11}
    , {region: 'Auckland', year: 2009, data_val: 8811.61}
    , {region: 'Auckland', year: 2010, data_val: 9179.95}
    , {region: 'Auckland', year: 2011, data_val: 8802.3}
    , {region: 'Auckland', year: 2012, data_val: 8632.36}
    , {region: 'Auckland', year: 2013, data_val: 9188.21}
    , {region: 'Auckland', year: 2014, data_val: 9124.06}
    , {region: 'Auckland', year: 2015, data_val: 9378.49}
    , {region: 'Auckland', year: 2016, data_val: 8924.01}
    , {region: 'Auckland', year: 2017, data_val: 9227.81}
    , {region: 'Auckland', year: 2018, data_val: 9066.95}
    , {region: 'Wellington', year: 2007, data_val: 2104.55}
    , {region: 'Wellington', year: 2008, data_val: 2083.76}
    , {region: 'Wellington', year: 2009, data_val: 2015.68}
    , {region: 'Wellington', year: 2010, data_val: 1991.45}
    , {region: 'Wellington', year: 2011, data_val: 1959.58}
    , {region: 'Wellington', year: 2012, data_val: 1904.3}
    , {region: 'Wellington', year: 2013, data_val: 1930.96}
    , {region: 'Wellington', year: 2014, data_val: 1877.3}
    , {region: 'Wellington', year: 2015, data_val: 1954.41}
    , {region: 'Wellington', year: 2016, data_val: 1941.23}
    , {region: 'Wellington', year: 2017, data_val: 2021.29}
    , {region: 'Wellington', year: 2018, data_val: 2013.4}
    , {region: 'Waikato', year: 2007, data_val: 6420.14}
    , {region: 'Waikato', year: 2008, data_val: 8395.6}
    , {region: 'Waikato', year: 2009, data_val: 7066.07}
    , {region: 'Waikato', year: 2010, data_val: 6119.89}
    , {region: 'Waikato', year: 2011, data_val: 6349.34}
    , {region: 'Waikato', year: 2012, data_val: 7461.5}
    , {region: 'Waikato', year: 2013, data_val: 6452.66}
    , {region: 'Waikato', year: 2014, data_val: 6209.08}
    , {region: 'Waikato', year: 2015, data_val: 6261.78}
    , {region: 'Waikato', year: 2016, data_val: 5541.36}
    , {region: 'Waikato', year: 2017, data_val: 5790.75}
    , {region: 'Waikato', year: 2018, data_val: 5828.51}]

const values = {handleForce};

const parseOptions = {
    header: true,
    dynamicTyping: true,
    skipEmptyLines: true,
    transformHeader: header =>
        header
            .toLowerCase()
            .replace(/\W/g, '_')
}

class Reader extends Component {
    state = {
        message: "Enter file",
    }

    render() {

        const showMessage = () => {
            this.setState({message: <Plot data={list}/>})
        }
        return (

            <div className="container">
                <CSVReader
                    cssClass="csv-reader-input"
                    label="Select CSV file"
                    onFileLoaded={showMessage}
                    onError={this.handleDarkSideForce}
                    parserOptions={parseOptions}
                    inputId="ObiWan"
                    inputName="ObiWan"
                    inputStyle={{color: 'blue'}}
                />

                {this.state.message}
            </div>
        );
    }
}


export default Reader