import React, {Component} from 'react';
import CanvasJSReact from './lib/canvasjs.react';

let CanvasJSChart = CanvasJSReact.CanvasJSChart;

// data from:
// https://www.coretennis.net/tennis-player/roger-federer/1/ranking.html
// https://www.coretennis.net/tennis-player/agnieszka-radwanska/5002/ranking.html

class Plot extends Component {

    constructor(props) {
        super(props);
        this.state = {
            data: "",
            toggleAuckland: true,
            toggleWaikato: true,
            toggleWellington: true,
            buttonColourAuckland: true,
            buttonColourWellington: true,
            buttonColourWaikato: true
        }
    }

    toggleAuckland() {
        this.setState({toggleAuckland: !this.state.toggleAuckland, buttonColourAuckland: !this.state.buttonColourAuckland})
    }

    toggleWaikato() {
        this.setState({toggleWaikato: !this.state.toggleWaikato, buttonColourWaikato: !this.state.buttonColourWaikato})
    }

    toggleWellington() {
        this.setState({toggleWellington: !this.state.toggleWellington, buttonColourWellington: !this.state.buttonColourWellington})
    }

    render() {
        let btn_class_auckland = this.state.buttonColourAuckland ? "grayButton" : "darkGrayButton";
        let btn_class_wellington = this.state.buttonColourWellington ? "grayButton" : "darkGrayButton";
        let btn_class_waikato = this.state.buttonColourWaikato ? "grayButton" : "darkGrayButton";
        const pointsAuckland = []
        const pointsWellington = []
        const pointsWaikato = []
        this.props.data.forEach(d => {
            if (d.region === 'Auckland') pointsAuckland.push({x: d.year, y: d.data_val})
            if (d.region === 'Waikato') pointsWaikato.push({x: d.year, y: d.data_val})
            if (d.region === 'Wellington') pointsWellington.push({x: d.year, y: d.data_val})
        })

        const options = {

            zoomEnabled: true,
            theme: "light2",
            animationDuration: 2000,
            animationEnabled: true,
            title: {
                text: "Poziom emisji dwutlenku węgla w Nowej Zelandii w latach 2007-2018",
                fontColor: "#2F4F4F"
            },
            axisX: {
                valueFormatString: "####",
                labelAngle: -45,
                title: "Rok",
                interlacedColor: "#F0F8FF"
            },
            axisY: {
                title: "Poziom emisji"
            },
            data:
                [{
                    visible: this.state.toggleAuckland,
                    type: "line",
                    legendText: "Auckland",
                    showInLegend: true,
                    markerType: "circle",
                    dataPoints: pointsAuckland
                }, {
                    visible: this.state.toggleWaikato,
                    type: "line",
                    legendText: "Waikato",
                    showInLegend: true,
                    markerType: "circle",
                    dataPoints: pointsWaikato
                }, {
                    visible: this.state.toggleWellington,
                    type: "line",
                    legendText: "Wellington",
                    showInLegend: true,
                    markerType: "circle",
                    dataPoints: pointsWellington
                }
                ]
        }


        return (

            <div>
                <button onClick={this.toggleAuckland.bind(this)} className={btn_class_auckland}> Toggle Auckland</button>
                <button onClick={this.toggleWaikato.bind(this)} className={btn_class_waikato}> Toggle Waikato</button>
                <button onClick={this.toggleWellington.bind(this)} className={btn_class_wellington}> Toggle Wellington</button>
                <CanvasJSChart options={options} className="plot"
                />
            </div>
        );
    }
}

export default Plot;